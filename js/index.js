// Global Variables
let map;

let therapists = [
	{ lat: -31.56391, lng: 147.154312, name: "Loc1" },
	{ lat: -33.718234, lng: 150.363181, name: "Loc2" },
	{ lat: -33.727111, lng: 150.371124, name: "Loc3" },
	{ lat: -33.848588, lng: 151.209834, name: "Loc4" },
	{ lat: -33.851702, lng: 151.216968, name: "Loc5" },
	{ lat: -34.671264, lng: 150.863657, name: "Loc6" },
	{ lat: -35.304724, lng: 148.662905, name: "Loc7" },
	{ lat: -36.817685, lng: 175.699196, name: "Loc8" },
	{ lat: -36.828611, lng: 175.790222, name: "Loc9" },
	{ lat: -37.75, lng: 145.116667, name: "Loc10" },
	{ lat: -37.759859, lng: 145.128708, name: "Loc11" },
	{ lat: -37.765015, lng: 145.133858, name: "Loc12" },
	{ lat: -37.770104, lng: 145.143299, name: "Loc13" },
	{ lat: -37.7737, lng: 145.145187, name: "Loc14" },
	{ lat: -37.774785, lng: 145.137978, name: "Loc15" },
	{ lat: -37.819616, lng: 144.968119, name: "Loc16" },
	{ lat: -38.330766, lng: 144.695692, name: "Loc17" },
	{ lat: -39.927193, lng: 175.053218, name: "Loc18" },
	{ lat: -41.330162, lng: 174.865694, name: "Loc19" },
	{ lat: -42.734358, lng: 147.439506, name: "Loc20" },
	{ lat: -42.734358, lng: 147.501315, name: "Loc21" },
	{ lat: -42.735258, lng: 147.438, name: "Loc22" },
	{ lat: -43.999792, lng: 170.463352, name: "Loc23" },
];


// Setting up autocomplete searchbar for addresses on map
function initAutocomplete() {
	// Create the search box and link it to the UI element.
	const input = document.getElementById("pac-input");
	const searchBox = new google.maps.places.SearchBox(input);
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	// Bias the SearchBox results towards current map's viewport.
	map.addListener("bounds_changed", () => {
		searchBox.setBounds(map.getBounds());
	});
	let markers = [];
	// Listen for the event fired when the user selects a prediction and retrieve
	// more details for that place.
	searchBox.addListener("places_changed", () => {
		const places = searchBox.getPlaces();
	
		if (places.length == 0) {
			return;
		}
		// Clear out the old markers.
		markers.forEach((marker) => {
			marker.setMap(null);
		});
		markers = [];
		// For each place, get the icon, name and location.
		const bounds = new google.maps.LatLngBounds();
		places.forEach((place) => {
			if (!place.geometry) {
				console.log("Returned place contains no geometry");
				return;
			}
			const icon = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(25, 25),
			};
			// Create a marker for each place.
			markers.push(
				new google.maps.Marker({
					map,
					icon,
					title: place.name,
					position: place.geometry.location,
				})
			);

			if (place.geometry.viewport) {
				// Only geocodes have viewport.
				bounds.union(place.geometry.viewport);
			} else {
				bounds.extend(place.geometry.location);
			}
		});
		map.fitBounds(bounds);
	});
}

// setting up our map
function initialize() {
	const googleMap = new google.maps.Map(document.getElementById("map"), {
		zoom: 3,
		center: { lat: -28.024, lng: 140.887 },
	});
	map = googleMap;
	getTherapists();
	updateMap();
	initAutocomplete();
}