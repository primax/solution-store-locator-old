// we will be getting all Therapist users via a fetch request
function getTherapists() {
	fetch("https://website.solutionagency.net/wp-json/wp/v2/posts", {
		headers: new Headers({
			'Authorization': 'Bearer YOUR_JWT_AUTH_TOKEN'
		})
	})
	.then(async response => {
		const newTherapists = await response.json();

		// check for error response
		if (!response.ok) {
			// get error message from body or default to response statusText
			const error = (data && data.message) || response.statusText;
			return Promise.reject(error);
		}

		newTherapists.forEach(nt => {
			let newTherapist = {
				lat: Number(nt.meta.latitude[0]), 
				lng: Number(nt.meta.longitude[0]), 
				name: nt.name,
				street_1: nt.meta.street_address_1[0],
				street_2: nt.meta.street_address_2[0],
				city: nt.meta.city[0],
				state: nt.meta.state[0],
				zip_code: nt.meta.zip_code[0],
				phone: nt.meta.phone[0],
				email: nt.meta.email[0],
				website: nt.meta.website[0],
			}
			therapists.push(newTherapist);
		});
		updateMap();
	})
	
	.catch(error => {
		console.error("There is an error- ", error);
	});
}

// we will be updating our map list and markers when the list of therapists have changed
function updateMap() {
	let mapList = document.getElementById("map-list");
	// clearing out current list
	mapList.innerHTML = '';

	const markers = therapists.map((therapist, i) => {
		const infoWindowContent = `
			<strong>${therapist.name}</strong>
			<p>${ therapist.street_2 ? `${therapist.street_1}, ${therapist.street_2}<br>${therapist.city}, ${location.state} ${therapist.zip_code}` : `${therapist.street_1}<br>${therapist.city}, ${therapist.state} ${therapist.zip_code}` }</p>
			${ therapist.phone ? `<span>Phone: <a href="tel:${therapist.phone}">${therapist.phone}</a></span><br></br>` : `` }
			${ therapist.email ? `<span>Email: <a href="mailto:${therapist.email}">${therapist.email}</a></span><br>` : `` }
			${ therapist.website ? `<span>Website: <a href="${therapist.website}" target="_blank">${therapist.website}</a></span>` : `` }
		`;
		const infowindow = new google.maps.InfoWindow({
			content: infoWindowContent,
		});

		const marker = new google.maps.Marker({
			position: therapist,
		});
		marker.addListener("click", () => {
			infowindow.open(map, marker);
		});

		let mapListItem = document.createElement("li")
		mapListItem.innerHTML = `
			<a href="#" onclick="centerMap(${therapist.lat}, ${therapist.lng})">
				<h2>${therapist.name}</h2>
				<p>Hello, this is a test</p>
			</a>
		`;
		mapListItem.addEventListener("click", () => {
			infowindow.open(map, marker);
		});
		mapList.appendChild(mapListItem);

		return marker;
	});
	new MarkerClusterer(map, markers, {
		imagePath: "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
	});
}

// when a map list item is clicked on, we will re-focus the map with a lat, lon, and zoom
function centerMap(lat, lon) {
	map.setCenter({lat: lat, lng: lon});
	map.setZoom(8);
}